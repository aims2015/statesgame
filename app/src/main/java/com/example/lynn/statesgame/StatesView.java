package com.example.lynn.statesgame;

import android.content.Context;
import android.widget.RelativeLayout;

import static com.example.lynn.statesgame.MainActivity.*;

/**
 * Created by lynn on 6/25/2015.
 */
public class StatesView extends RelativeLayout {

    public StatesView(Context context) {
        super(context);

        for (int counter=0;counter<states.length;counter++) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

            states[counter].setLayoutParams(layoutParams);

            states[counter].setTranslationX(100 + counter*600);

            addView(states[counter]);
        }

    }

}
